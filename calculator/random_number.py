import random


def generate():
    a = 1
    b = 90234523567622345
    numbers = range(a, b)

    return random.choice(numbers)

