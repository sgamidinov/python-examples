from flask import Flask, request, render_template, after_this_request

from calculator import calculator, random_number

app = Flask(__name__)


@app.route('/')
def index():
    @after_this_request
    def set_auth_cookie(response):
        response.headers.set('Authorization', random_number.generate())
        response.set_cookie()
        return response
    return render_template("index.html")


@app.route('/divide', methods=['POST', 'GET'])
def server_divide():
    data = request.form
    x = int(data.get('x') or 1)
    y = int(data.get('y') or 1)
    return render_template("calculator.html", result=calculator.divide(x, y), x=x, y=y, action="divide")


@app.route('/multiply', methods=['POST', 'GET'])
def server_multiply():
    data = request.form
    x = int(data.get('x') or 1)
    y = int(data.get('y') or 1)
    return render_template("calculator.html", result=calculator.multiply(x, y), x=x, y=y, action="multiply")


@app.route('/random-number')
def server_random_number():
    return str(random_number.generate())


if __name__ == '__main__':
    app.run()
